package com.bbva.cnmc.proxyaso.controller;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.http.client.*;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.bbva.cnmc.proxyaso.dto.aso.AsoDTO;
import com.bbva.cnmc.proxyaso.exception.ServicioException;
import com.bbva.cnmc.proxyaso.service.impl.GrantingTicketServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller("proxyASOController")
public class ProxyASOController {

	@Autowired
	private GrantingTicketServiceImpl grantingTicektService;

	private Logger logger = Logger.getLogger(ProxyASOController.class);

	/**
	* Método que procesa la petición tipo GET, se obtiene la respuesta y se
	* transforma inicialmente en un String con la información del response.
	* 
	* @param parametrosAsoDTO
	* @return
	* @throws Exception
	*/
	@SuppressWarnings("unchecked")
	public String procesarPeticionGetHTTP(AsoDTO parametrosAsoDTO) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		logger.info("1");
		String peticionObtenida = StringUtils.EMPTY;
		logger.info("2");
		try {
			HttpHeaders headers = createHttpHeaders(parametrosAsoDTO);
			HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			logger.info("3");
			ResponseEntity<String> response = restTemplate.exchange(parametrosAsoDTO.getAsoBaseUrl(), HttpMethod.GET,
					entity, String.class);
			logger.info("4");

			

			peticionObtenida = response.getBody();
		} catch (HttpServerErrorException e) {
			Map<String, String> errorMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
			logger.info("6");
			throw new ServicioException(errorMap.get("message"), new Throwable(errorMap.get("stacktrace")));
			
		}


		
		
		return peticionObtenida;
	}
	
	/**
	* Método que procesa la petición tipo PATCH, aplica modificaciones parciales a un recurso,
	* transforma un String entrante actualizando datos con la información del response.
	* 
	* @param parametrosAsoDTO
	* @return String peticionObtenida
	* @throws Exception
	*/
	@SuppressWarnings("unchecked")
	public String procesarPeticionPatchHTTP(AsoDTO parametrosAsoDTO) throws Exception {
		
		System.out.println("Entro a ejecutar petición patch");
		HttpClient client = HttpClients.createDefault();
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(client));
		
		String peticionObtenida = StringUtils.EMPTY;
		try {
			
			HttpHeaders headers = createHttpHeaders(parametrosAsoDTO);
						
			System.out.println("header: "+headers.toString());
			
			HttpEntity<Object> entity = new HttpEntity<Object>(parametrosAsoDTO.getBody(),headers);			
			ResponseEntity<String> response = restTemplate.exchange(parametrosAsoDTO.getAsoBaseUrl(), HttpMethod.PATCH,
					entity, String.class);
			
			System.out.println("Result - status ("+ response.getStatusCode() + ") has body: " + response.hasBody());
			System.out.println("Body: " + response.getBody());
			
			peticionObtenida = response.getBody();
		} catch (HttpServerErrorException e) {
			Map<String, String> errorMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
			throw new ServicioException(errorMap.get("message"), new Throwable(errorMap.get("stacktrace")));
		}
		return peticionObtenida;
	}

	private HttpHeaders createHttpHeaders(AsoDTO parametrosAsoDTO) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(parametrosAsoDTO.getGrantingTicketDTO().getGrantingTicketMediaType());
		headers.add("tsec", generarGrantingTicket(parametrosAsoDTO));
		headers.add("iv-user", parametrosAsoDTO.getGrantingTicketDTO().getIvUser());
		headers.add("iv_ticketservice", parametrosAsoDTO.getGrantingTicketDTO().getIvTicket());
		return headers;
	}

	/**
	* Método que procesa la petición tipo POST, se obtiene la respuesta y se
	* transforma inicialmente en un String con la información del response.
	* 
	* @param parametrosAsoDTO
	* @return
	* @throws Exception
	*/
	@SuppressWarnings("unchecked")
	public String procesarPeticionPostHTTP(AsoDTO parametrosAsoDTO) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(parametrosAsoDTO.getGrantingTicketDTO().getGrantingTicketMediaType());
		headers.add("tsec", generarGrantingTicket(parametrosAsoDTO));
		headers.add("iv-user", parametrosAsoDTO.getGrantingTicketDTO().getIvUser());
		headers.add("iv_ticketservice", parametrosAsoDTO.getGrantingTicketDTO().getIvTicket());
		
		

		HttpEntity<Object> entity = new HttpEntity<Object>(parametrosAsoDTO.getBody(), headers);
		
		System.out.print("Entity: "+ entity.toString());
		
		String peticionObtenida = StringUtils.EMPTY;
		try {
			peticionObtenida = restTemplate.postForObject(parametrosAsoDTO.getAsoBaseUrl(), entity, String.class);
		} catch (HttpServerErrorException e) {
			Map<String, String> errorMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
			throw new ServicioException(errorMap.get("message"), new Throwable(errorMap.get("stacktrace")));
		}
		return peticionObtenida;
	}
	
	/**
	* Método que procesa la petición tipo POST, se obtiene la respuesta y se
	* transforma inicialmente en un String con la información del response.
	* 
	* @param parametrosAsoDTO
	* @return
	* @throws Exception
	*/
	@SuppressWarnings("unchecked")
	public String procesarPeticionPutHTTP(AsoDTO parametrosAsoDTO) throws Exception {
		System.out.println("Entro a ejecutar petición put");
		
		RestTemplate restTemplate = new RestTemplate();
		
		
		String peticionObtenida = StringUtils.EMPTY;
		try {
			
			HttpHeaders headers = createHttpHeaders(parametrosAsoDTO);
						
			System.out.println("header: "+headers.toString());
			
			HttpEntity<Object> entity = new HttpEntity<Object>(parametrosAsoDTO.getBody(),headers);			
			ResponseEntity<String> response = restTemplate.exchange(parametrosAsoDTO.getAsoBaseUrl(), HttpMethod.PUT,
					entity, String.class);
			
			System.out.println("Result - status ("+ response.getStatusCode() + ") has body: " + response.hasBody());
			System.out.println("Body: " + response.getBody());
			
			peticionObtenida = response.getBody();
		} catch (HttpServerErrorException e) {
			Map<String, String> errorMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
			throw new ServicioException(errorMap.get("message"), new Throwable(errorMap.get("stacktrace")));
		}
		return peticionObtenida;
	}

	
	
	public String generarGrantingTicket(AsoDTO parametrosAsoDTO) throws Exception {
		return grantingTicektService.generateTSEC(parametrosAsoDTO);
	}
}
				