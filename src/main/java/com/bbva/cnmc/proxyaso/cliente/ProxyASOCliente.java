package com.bbva.cnmc.proxyaso.cliente;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.bbva.cnmc.proxyaso.controller.ProxyASOController;
import com.bbva.cnmc.proxyaso.dto.aso.AsoDTO;
import com.bbva.cnmc.proxyaso.dto.aso.GrantingTicketDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Programa principal que sirve para ejecutar los Web-Services ASO del 
 * Banco BBVA.
 * 
 * @author CE53233
 *
 */
public class ProxyASOCliente {

	final static Logger logger = Logger.getLogger(ProxyASOCliente.class);
	static ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

	public static void main(String[] args) throws Exception {
//		pruebaUnitariaConsumoServicioGetASO();
//		pruebaUnitariaConsumoServicioPatchASO();
//		pruebaUnitariaConsumoServicioPostASO();
//		pruebaUnitariaConsumoServicioPutASO();
	}

	/**
	 * Prueba de Unidad No. 1: Consumo de Servicio Web ASO - Tipo GET
	 * 
	 * - Se define la URL del servicio a consumir y se adicionan los parámetros para el Granting Ticket.
	 * - Se inyectan los datos de usuario del banco y se define el tipo de operación (Se aclara que
	 *   para obtener el GrantingTicket se consume el servicio POST ya definido en el desarrollo anterior.
	 *   
	 *   En el atrituto ASO se carga la URL, se define el tipo de información (JSON) y el tipo de operación (GET).
	 *   
	 * @throws Exception
	 */
	public static void pruebaUnitariaConsumoServicioGetASO() throws Exception {
		logger.info("Se inicializa la invocación del servicio ASO.");
		GrantingTicketDTO grantingTicketDTO = (GrantingTicketDTO) context.getBean("grantingTicketDTO");
		grantingTicketDTO.setUrlGrantingTicketService("http://150.250.240.131:1600/TechArchitecture/co/grantingTicket/V02");
		grantingTicketDTO.setConsumerID("12000025");
		grantingTicketDTO.setAuthenticationType("00");
		grantingTicketDTO.setAuthenticationData("iv_ticketService");
		grantingTicketDTO.setIvUser("CE52000");
		grantingTicketDTO.setIvTicket("VMfCeHr9IAPlT2p5Yf2EAiT1WikFCZE9Bmc4Ow209qIUx5utkxt/pQ==");
		grantingTicketDTO.setOperacionHttpServicioGrantingTicket(HttpMethod.POST);
		grantingTicketDTO.setGrantingTicketMediaType(MediaType.APPLICATION_JSON);

		/**
		 * Se cargan los parámetros del servicio tipo tipo GET 
		 */
		AsoDTO asoDto = (AsoDTO) context.getBean("asoDto");
		asoDto.setGrantingTicketDTO(grantingTicketDTO);
		asoDto.setAsoBaseUrl("http://150.250.240.131:1600/sarlaft/V01/sarlaft?$filter=(documentType==CC;documentId==000000234234234;checkDigit==0;surname=='';secondSurname=='';name=='')");
		asoDto.setAsoMediaType(MediaType.APPLICATION_JSON);
		asoDto.setOperacionHttpServicio(HttpMethod.GET);

		ProxyASOController controller = (ProxyASOController) context.getBean("proxyASOController");
		String peticionHttp = controller.procesarPeticionGetHTTP(asoDto);
		logger.debug("[RES-GET]: '" + peticionHttp + "'");
	}
	
	
		
	/**
	 * Prueba de Unidad No. 2: Consumo de Servicio Web ASO - Tipo PATCH
	 * 
	 * - Se define la URL del servicio a consumir y se adicionan los parámetros para el Granting Ticket.
	 * - Se inyectan los datos de usuario del banco y se define el tipo de operación (Se aclara que
	 *   para obtener el GrantingTicket se consume el servicio POST ya definido en el desarrollo anterior.
	 *   
	 *   En el atrituto ASO se carga la URL, se define el tipo de información (JSON) y el tipo de operación (PATCH).
	 *   
	 *   EN el método del controller se inyectan los datos como Header.
	 *   
	 * @throws Exception
	 */
	public static void pruebaUnitariaConsumoServicioPatchASO() throws Exception {
		logger.info("Se inicializa la invocación del servicio ASO.");
		GrantingTicketDTO grantingTicketDTO = (GrantingTicketDTO) context.getBean("grantingTicketDTO");
		grantingTicketDTO.setUrlGrantingTicketService("http://150.250.240.131:1600/TechArchitecture/co/grantingTicket/V02");
		grantingTicketDTO.setConsumerID("12000025");
		grantingTicketDTO.setAuthenticationType("00");
		grantingTicketDTO.setAuthenticationData("iv_ticketService");
		grantingTicketDTO.setIvUser("CE52000");
		grantingTicketDTO.setIvTicket("VMfCeHr9IAPlT2p5Yf2EAiT1WikFCZE9Bmc4Ow209qIUx5utkxt/pQ==");
		grantingTicketDTO.setOperacionHttpServicioGrantingTicket(HttpMethod.POST);
		grantingTicketDTO.setGrantingTicketMediaType(MediaType.APPLICATION_JSON);

		/**
		 * Se cargan los parámetros del servicio tipo tipo PATCH
		 */
		AsoDTO asoDto = (AsoDTO) context.getBean("asoDto");
		asoDto.setGrantingTicketDTO(grantingTicketDTO);
		asoDto.setAsoBaseUrl("http://150.250.240.131:1600/customers/V00/customers/10000000029258220/interviews/1");
		asoDto.setAsoMediaType(MediaType.APPLICATION_JSON);
		asoDto.setOperacionHttpServicio(HttpMethod.PATCH);
		
		/**
		 * Se inicializa el mapper que recibirá de forma deserializasa cualquier información
		 * en formato JSON que requiera para el BODY (Servicios tipo PATCH).
		 */
		ObjectMapper mapper = new ObjectMapper(); 
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

		/**
		 * Se abre el archivo JSON que está en la carpeta /src/main/resources y se transforma en una lista para que reconozca cualquier
		 * tipo de información y se carga en el atributo [Body] y se convierte a lista.
		 */
		String fileName = "json_body/body_granting_ticket2.json";
        ClassLoader classLoader = new ProxyASOCliente().getClass().getClassLoader();
        File archivoJsonBody = new File(classLoader.getResource(fileName).getFile());
        Map readValue = mapper.readValue(archivoJsonBody, Map.class);
		asoDto.setBody(new ObjectMapper().writeValueAsString(readValue));
		
		
		System.out.println("ReadValue: " + readValue.toString());
		
		
		/**
		 * Se ejecuta la operación para obtener la respuesta del servicio
		 */
		ProxyASOController controller = (ProxyASOController) context.getBean("proxyASOController");
		String peticionHttp = controller.procesarPeticionPatchHTTP(asoDto);
		logger.debug("[RES-GET]: '" + peticionHttp + "'");
	}

	
	/**
	 * Prueba de Unidad No. 3: Consumo de Servicio Web ASO - Tipo POST
	 * 
	 * - Se define la URL del servicio a consumir y se adicionan los parámetros para el Granting Ticket.
	 * - Se inyectan los datos de usuario del banco y se define el tipo de operación (Se aclara que
	 *   para obtener el GrantingTicket se consume el servicio POST ya definido en el desarrollo anterior.
	 *   
	 *   En el atrituto ASO se carga la URL, se define el tipo de información (JSON) y el tipo de operación (POST).
	 *   
	 *   EN el método del controller se inyectan los datos como Header.
	 *   
	 * @throws Exception
	 */
	public static void pruebaUnitariaConsumoServicioPostASO() throws Exception {
		logger.info("Se inicializa la invocación del servicio ASO.");
		GrantingTicketDTO grantingTicketDTO = (GrantingTicketDTO) context.getBean("grantingTicketDTO");
		grantingTicketDTO.setUrlGrantingTicketService("http://150.250.240.131:1600/TechArchitecture/co/grantingTicket/V02");
		grantingTicketDTO.setConsumerID("12000025");
		grantingTicketDTO.setAuthenticationType("00");
		grantingTicketDTO.setAuthenticationData("iv_ticketService");
		grantingTicketDTO.setIvUser("CE52000");
		grantingTicketDTO.setIvTicket("VMfCeHr9IAPlT2p5Yf2EAiT1WikFCZE9Bmc4Ow209qIUx5utkxt/pQ==");
		grantingTicketDTO.setOperacionHttpServicioGrantingTicket(HttpMethod.POST);
		grantingTicketDTO.setGrantingTicketMediaType(MediaType.APPLICATION_JSON);

		/**
		 * Se cargan los datos del servicio Web a consumir y se adicionan los parámetros tipo POST.
		 */
		AsoDTO asoDto = (AsoDTO) context.getBean("asoDto");
		asoDto.setGrantingTicketDTO(grantingTicketDTO);
		asoDto.setAsoBaseUrl("http://150.250.240.131:1600/loan/V01/loan");
		asoDto.setAsoMediaType(MediaType.APPLICATION_JSON);
		asoDto.setOperacionHttpServicio(HttpMethod.POST);
		
		/**
		 * Se inicializa el mapper que recibirá de forma deserializasa cualquier información
		 * en formato JSON que requiera para el BODY (Servicios tipo POST).
		 */
		ObjectMapper mapper = new ObjectMapper(); 
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

		/**
		 * Se abre el archivo JSON que está en la carpeta /src/main/resources y se transforma en una lista para que reconozca cualquier
		 * tipo de información y se carga en el atributo [Body] y se convierte a lista.
		 */
		String fileName = "json_body/body_granting_ticket.json";
        ClassLoader classLoader = new ProxyASOCliente().getClass().getClassLoader();
        File archivoJsonBody = new File(classLoader.getResource(fileName).getFile());
        Map readValue = mapper.readValue(archivoJsonBody, Map.class);
		asoDto.setBody(new ObjectMapper().writeValueAsString(readValue));
		
		
		System.out.println("ReadValue: " + readValue.toString());
		
		/**
		 * Se ejecuta la operación para obtener la respuesta del servicio
		 */
		ProxyASOController controller = (ProxyASOController) context.getBean("proxyASOController");
		String peticionHttp = controller.procesarPeticionPostHTTP(asoDto);
		logger.debug("[RES-POST]: '" + peticionHttp + "'");
	}
	
	/**
	 * Prueba de Unidad No. 4: Consumo de Servicio Web ASO - Tipo PUT
	 * 
	 * - Se define la URL del servicio a consumir y se adicionan los parámetros para el Granting Ticket.
	 * - Se inyectan los datos de usuario del banco y se define el tipo de operación (Se aclara que
	 *   para obtener el GrantingTicket se consume el servicio PUT ya definido en el desarrollo anterior.
	 *   
	 *   
	 *   En el atrituto ASO se carga la URL, se define el tipo de información (JSON) y el tipo de operación (PUT).
	 *   
	 *   EN el método del controller se inyectan los datos como Header.
	 *   
	 * @throws Exception
	 */
	public static void pruebaUnitariaConsumoServicioPutASO() throws Exception {
		logger.info("Se inicializa la invocación del servicio ASO.");
		GrantingTicketDTO grantingTicketDTO = (GrantingTicketDTO) context.getBean("grantingTicketDTO");
		grantingTicketDTO.setUrlGrantingTicketService("http://150.250.240.131:1600/TechArchitecture/co/grantingTicket/V02");
		grantingTicketDTO.setConsumerID("12000025");
		grantingTicketDTO.setAuthenticationType("00");
		grantingTicketDTO.setAuthenticationData("iv_ticketService");
		grantingTicketDTO.setIvUser("CE52000");
		grantingTicketDTO.setIvTicket("VMfCeHr9IAPlT2p5Yf2EAiT1WikFCZE9Bmc4Ow209qIUx5utkxt/pQ==");
		grantingTicketDTO.setOperacionHttpServicioGrantingTicket(HttpMethod.POST);
		grantingTicketDTO.setGrantingTicketMediaType(MediaType.APPLICATION_JSON);

		/**
		 * Se cargan los datos del servicio Web a consumir y se adicionan los parámetros tipo POST.
		 */
		AsoDTO asoDto = (AsoDTO) context.getBean("asoDto");
		asoDto.setGrantingTicketDTO(grantingTicketDTO);
		asoDto.setAsoBaseUrl("http://localhost:12020/testPut/test");
		asoDto.setAsoMediaType(MediaType.APPLICATION_JSON);
		asoDto.setOperacionHttpServicio(HttpMethod.PUT);
		
		/**
		 * Se inicializa el mapper que recibirá de forma deserializasa cualquier información
		 * en formato JSON que requiera para el BODY (Servicios tipo PUT).
		 */
		ObjectMapper mapper = new ObjectMapper(); 
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

		/**
		 * Se abre el archivo JSON que está en la carpeta /src/main/resources y se transforma en una lista para que reconozca cualquier
		 * tipo de información y se carga en el atributo [Body] y se convierte a lista.
		 */
		String fileName = "json_body/body_granting_ticket.json";
        ClassLoader classLoader = new ProxyASOCliente().getClass().getClassLoader();
        File archivoJsonBody = new File(classLoader.getResource(fileName).getFile());
        Map readValue = mapper.readValue(archivoJsonBody, Map.class);
		asoDto.setBody(new ObjectMapper().writeValueAsString(readValue));
		
		
		System.out.println("ReadValue: " + readValue.toString());
		
		/**
		 * Se ejecuta la operación para obtener la respuesta del servicio
		 */
		ProxyASOController controller = (ProxyASOController) context.getBean("proxyASOController");
		String peticionHttp = controller.procesarPeticionPutHTTP(asoDto);
		logger.debug("[RES-POST]: '" + peticionHttp + "'");
	}

}
