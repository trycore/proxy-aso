package com.bbva.cnmc.proxyaso.dto.tsec;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class BackendUserRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userId;

	private String accessCode;

	private String dialogId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getDialogId() {
		return dialogId;
	}

	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}