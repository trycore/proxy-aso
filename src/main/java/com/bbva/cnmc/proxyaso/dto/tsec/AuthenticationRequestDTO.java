package com.bbva.cnmc.proxyaso.dto.tsec;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AuthenticationRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -725568443641822414L;

	private AuthenticationDTO authentication;

	private BackendUserRequestDTO backendUserRequest;

	public AuthenticationDTO getAuthentication() {
		return authentication;
	}

	public void setAuthentication(AuthenticationDTO authentication) {
		this.authentication = authentication;
	}

	public BackendUserRequestDTO getBackendUserRequest() {
		return backendUserRequest;
	}

	public void setBackendUserRequest(BackendUserRequestDTO backendUserRequest) {
		this.backendUserRequest = backendUserRequest;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
