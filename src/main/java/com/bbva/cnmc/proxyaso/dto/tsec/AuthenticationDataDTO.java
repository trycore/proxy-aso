package com.bbva.cnmc.proxyaso.dto.tsec;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AuthenticationDataDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2842399990241735051L;

	private String idAuthenticationData;

	private List<String> authenticationData;

	public String getIdAuthenticationData()
	{
		return idAuthenticationData;
	}

	public void setIdAuthenticationData(String idAuthenticationData)
	{
		this.idAuthenticationData = idAuthenticationData;
	}

	public List<String> getAuthenticationData()
	{
		return authenticationData;
	}

	public void setAuthenticationData(List<String> authenticationData)
	{
		this.authenticationData = authenticationData;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}