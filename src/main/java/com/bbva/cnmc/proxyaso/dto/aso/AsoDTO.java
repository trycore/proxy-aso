package com.bbva.cnmc.proxyaso.dto.aso;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public class AsoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String asoBaseUrl;
	private HttpMethod operacionHttpServicio;
	private MediaType asoMediaType;
	private Object body;

	private GrantingTicketDTO grantingTicketDTO;

	public AsoDTO() {
		super();
	}

	public String getAsoBaseUrl() {
		return asoBaseUrl;
	}

	public void setAsoBaseUrl(String asoBaseUrl) {
		this.asoBaseUrl = asoBaseUrl;
	}

	public HttpMethod getOperacionHttpServicio() {
		return operacionHttpServicio;
	}

	public void setOperacionHttpServicio(HttpMethod operacionHttpServicio) {
		this.operacionHttpServicio = operacionHttpServicio;
	}

	public MediaType getAsoMediaType() {
		return asoMediaType;
	}

	public void setAsoMediaType(MediaType asoMediaType) {
		this.asoMediaType = asoMediaType;
	}

	public GrantingTicketDTO getGrantingTicketDTO() {
		return grantingTicketDTO;
	}

	public void setGrantingTicketDTO(GrantingTicketDTO grantingTicketDTO) {
		this.grantingTicketDTO = grantingTicketDTO;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
