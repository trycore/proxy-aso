package com.bbva.cnmc.proxyaso.dto.aso;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public class GrantingTicketDTO implements Serializable	{

	private static final long serialVersionUID = 1L;

	private String urlGrantingTicketService;
	private HttpMethod operacionHttpServicioGrantingTicket;
	private MediaType grantingTicketMediaType;

	private String consumerID;
	private String authenticationType;
	private String authenticationData;
	private String ivUser;
	private String ivTicket;

	public GrantingTicketDTO() {
		super();
	}

	public String getUrlGrantingTicketService() {
		return urlGrantingTicketService;
	}

	public void setUrlGrantingTicketService(String urlGrantingTicketService) {
		this.urlGrantingTicketService = urlGrantingTicketService;
	}

	public HttpMethod getOperacionHttpServicioGrantingTicket() {
		return operacionHttpServicioGrantingTicket;
	}

	public void setOperacionHttpServicioGrantingTicket(HttpMethod operacionHttpServicioGrantingTicket) {
		this.operacionHttpServicioGrantingTicket = operacionHttpServicioGrantingTicket;
	}

	public MediaType getGrantingTicketMediaType() {
		return grantingTicketMediaType;
	}

	public void setGrantingTicketMediaType(MediaType grantingTicketMediaType) {
		this.grantingTicketMediaType = grantingTicketMediaType;
	}

	public String getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}

	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}

	public String getAuthenticationData() {
		return authenticationData;
	}

	public void setAuthenticationData(String authenticationData) {
		this.authenticationData = authenticationData;
	}

	public String getIvUser() {
		return ivUser;
	}

	public void setIvUser(String ivUser) {
		this.ivUser = ivUser;
	}

	public String getIvTicket() {
		return ivTicket;
	}

	public void setIvTicket(String ivTicket) {
		this.ivTicket = ivTicket;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}