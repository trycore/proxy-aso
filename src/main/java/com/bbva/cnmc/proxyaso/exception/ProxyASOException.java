package com.bbva.cnmc.proxyaso.exception;

public class ProxyASOException extends Exception {
	private static final long serialVersionUID = 9046642504770253492L;

	public ProxyASOException() {
		super();
	}

	public ProxyASOException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProxyASOException(String message) {
		super(message);
	}

	public ProxyASOException(Throwable cause) {
		super(cause);
	}
}