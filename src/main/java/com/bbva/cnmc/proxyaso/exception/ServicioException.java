package com.bbva.cnmc.proxyaso.exception;

public class ServicioException extends Exception {
	private static final long serialVersionUID = 147426630470909986L;

	public ServicioException() {
		super();
	}

	public ServicioException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServicioException(String message) {
		super(message);
	}

	public ServicioException(Throwable cause) {
		super(cause);
	}
}