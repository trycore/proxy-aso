package com.bbva.cnmc.proxyaso.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.bbva.cnmc.proxyaso.dto.aso.AsoDTO;
import com.bbva.cnmc.proxyaso.dto.tsec.AuthenticationDTO;
import com.bbva.cnmc.proxyaso.dto.tsec.AuthenticationDataDTO;
import com.bbva.cnmc.proxyaso.dto.tsec.AuthenticationRequestDTO;
import com.bbva.cnmc.proxyaso.dto.tsec.BackendUserRequestDTO;
import com.bbva.cnmc.proxyaso.exception.ProxyASOException;
import com.bbva.cnmc.proxyaso.service.GrantingTicketService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class GrantingTicketServiceImpl implements GrantingTicketService {

	private static final Log LOG = LogFactory.getLog(GrantingTicketServiceImpl.class);

	@SuppressWarnings("unchecked")
	public String generateTSEC(AsoDTO asoDTO) throws Exception {
		// Rest template	
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(asoDTO.getGrantingTicketDTO().getGrantingTicketMediaType());
		AuthenticationRequestDTO authenticationRequest = getAuthenticationRequest(asoDTO);

		HttpEntity<String> entity = new HttpEntity<String>(new ObjectMapper().writeValueAsString(authenticationRequest),
				headers);
		try {
			ResponseEntity<String> response = restTemplate.exchange(
					asoDTO.getGrantingTicketDTO().getUrlGrantingTicketService(),
					asoDTO.getGrantingTicketDTO().getOperacionHttpServicioGrantingTicket(), entity, String.class);			
			return response.getHeaders().getFirst("tsec");
		} catch (HttpServerErrorException e) {
			LOG.error(e.getMessage(), e);
			Map<String, String> errorMap = new ObjectMapper().readValue(e.getResponseBodyAsString(), Map.class);
			throw new ProxyASOException(errorMap.get("message"), new Throwable(errorMap.get("stacktrace")));
		}

	}

	private AuthenticationRequestDTO getAuthenticationRequest(AsoDTO asoDTO) throws Exception {
		AuthenticationDataDTO authenticationData = new AuthenticationDataDTO();
		authenticationData.setIdAuthenticationData(asoDTO.getGrantingTicketDTO().getAuthenticationData());
		authenticationData.setAuthenticationData(Arrays.asList(asoDTO.getGrantingTicketDTO().getIvTicket()));

		BackendUserRequestDTO backendUserRequest = new BackendUserRequestDTO();
		backendUserRequest.setUserId("");
		backendUserRequest.setAccessCode("");
		backendUserRequest.setDialogId("");

		AuthenticationDTO authentication = new AuthenticationDTO();
		authentication.setUserID(asoDTO.getGrantingTicketDTO().getIvUser());
		authentication.setConsumerID(asoDTO.getGrantingTicketDTO().getConsumerID());
		authentication.setAuthenticationType(asoDTO.getGrantingTicketDTO().getAuthenticationType());
		authentication.setAuthenticationData(Arrays.asList(authenticationData));

		AuthenticationRequestDTO authenticationRequest = new AuthenticationRequestDTO();
		authenticationRequest.setAuthentication(authentication);
		authenticationRequest.setBackendUserRequest(backendUserRequest);

		return authenticationRequest;
	}
}