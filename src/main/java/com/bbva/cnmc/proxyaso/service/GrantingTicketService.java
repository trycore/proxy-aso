package com.bbva.cnmc.proxyaso.service;

import com.bbva.cnmc.proxyaso.dto.aso.AsoDTO;

public interface GrantingTicketService {
	public String generateTSEC(AsoDTO asoDTO) throws Exception;
}
